// 1. setTimeout викликає функцію один раз через певний проміжок часу.
// setInterval викликає функцію регулярно повторюючи виклик через певний проміжок часу.

// 2. Значення за дефолтом 0, але функція  не спрацює миттєво. Спочатку виконається код (якщо він присутній), а потім максимально швидко
//  буде викликана функція.

// 3. Коли раніше створений цикл вже не потрібний необхідно викликати clearInterval щоб зупинити подальше виконання функції, для
//  зменшення навантаження.

let indexValue = 0;
function show() {
  let img = document.querySelectorAll(".image-to-show");
  let i;
  for (i = 0; i < img.length; i++) {
    img[i].style.display = "none";
  }
  indexValue++;
  if (indexValue > img.length) {
    indexValue = 1;
  }
  img[indexValue - 1].style.display = "block";
}
show();
let interval = setInterval(show, 3000);

function stop() {
  clearInterval(interval);
  interval = null;
}

function resume() {
  if (interval === null) {
    interval = setInterval(show, 3000);
  }
}

let btnStop = document.querySelector(".stop");
let btnResume = document.querySelector(".resume");
btnStop.addEventListener("click", stop);
btnResume.addEventListener("click", resume);
